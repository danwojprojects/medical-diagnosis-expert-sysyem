/*zalozenia:
	hierarchia tak zeby kazda choroba wynikala z objawow badz WCZESNIEJSZYCH chorob
	cf od -1 do 1
	jezeli kilka zestawow objawow definuja ta sama chorobe to maja byc jeden obs za drugim

*/

options mprint symbolgen;

%macro CF_OR(A,B);
             %sysfunc(ifn(&A.>0 and &B.>0,((&A.+&B.)+-1*(&A.*&B.)),
                           %sysfunc(ifn(&A.<0 and &B.<0,((&A.+&B.)+(&A.*&B.)),
                                        %sysfunc(ifn(&A.*&B.<0,(&A.+&B.)/(1-1*%sysfunc(min(%sysfunc(abs(&A.)),%sysfunc(abs(&B.))))),0))
                           ))
             ))
%mend;

%macro CF(val);
	%global tmp;
	%let n = %sysfunc(countw(&val.,#));
	%if &n. >= 2 %then %do;
		%let i =1;
		%let tmp=%scan(&val.,1,#);
		%let tmp2=%scan(&val.,2,#);
		%let tmp = %CF_OR(&tmp.,&tmp2.);
		%let i = 3;
		%do %while(&i. <= &n.);
			%let tmp2=%scan(&val.,&i.,#);
			%let tmp = %CF_OR(&tmp.,&tmp2.);
			%let i = %eval(&i.+1);
		%end;
	%end;
%mend;

%macro listCF(l);
	%let k = %sysfunc(countw(&l.,#));
	%if &k. >= 1 %then %do;
		%let j=1;
		%do %while (&j.<=&k.);
			%let t = %scan(&l.,&j.,#);
			%CF(&&&t.);
			%let &t. = &tmp.;
			%let j = %eval(&j.+1);
		%end;
	%end;
%mend;

%macro diagnozuj;
	data _null_;
		length tmpdiag tmplist tmplist2 $2000 tmpnazwa $20 p 8;
		set choroba end = eof;
		retain tmpdiag tmpnazwa tmplist tmplist2 p ;
		diagnoza=compbl(diagnoza);
		nazwa=compress(nazwa);
		if _n_ eq 1 then do;
			tmpdiag = diagnoza;
			tmpnazwa=nazwa;
			p=0;
			tmplist2=catx('#',tmplist2,nazwa);
		end;
		else do;
			if (compress(nazwa) = compress(tmpnazwa)) then do;
				tmpdiag=catx('#',tmpdiag,diagnoza);
				p+1;
				if (p = 1) then do;
					tmplist=catx('#',tmplist,tmpnazwa);
				end;

				if (eof=1) then do;
					call symputx(tmpnazwa,tmpdiag,'G');
					call symputx('list',tmplist,'G');
					call symputx('list2',tmplist2,'G');
				end;
			end;
			else do;
				call symputx(tmpnazwa,tmpdiag,'G');
				tmpnazwa=nazwa;
				tmpdiag=diagnoza;
				tmplist2=catx('#',tmplist2,nazwa);
				p=0;
				if (eof=1) then do;
					call symputx(tmpnazwa,tmpdiag,'G');
					call symputx('list',tmplist,'G');
					call symputx('list2',tmplist2,'G');
				end;
			end;
		end;
	run;
	
	data _null_;
		set objaw;
		call symputx(nazwa,czy_wystepuje,'G');
	run;
	
	%listCF(&list.);
	
	data diagnoza;
		length choroba $2000 diagnoza czyPrzekraczaCutoff 8;
		retain czyPrzekraczaCutoff 0;
		%let n = %sysfunc(countw(&list2.,#));
		%if (&n. >= 1) %then %do;
			%let i = 1;
			%do %while(&i. <= &n.);
				%let t = %scan(&list2.,&i.,#);
				choroba="&t.";
				diagnoza=&&&t.;
				if (diagnoza > &cutoff.) then czyPrzekraczaCutoff=1;
				call symputx('czyPrzekraczaCutoff',czyPrzekraczaCutoff,'G');
				output;
				%let i = %eval(&i.+1);
			%end;
		%end;
		keep choroba diagnoza;
	run;
	
	%if &czyPrzekraczaCutoff. %then %do;
		TITLE 'DIAGNOZA';
		proc sql;
			select choroba, diagnoza as pewnosc
			from diagnoza
			having diagnoza = max(diagnoza)
			;
		quit;
	%end;
	%else %do;
		TITLE 'BRAK DIAGNOZY';
		proc sql outobs=1;
			select "na podstawie wskazanych objawów diagnoza okazała się niemożliwa" as szczegoly from dictionary.members where libname = 'WORK';
		quit;
	%end;
%mend;
%diagnozuj;